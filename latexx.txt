\documentclass{beamer}
%
% Choose how your presentation looks.
%
% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
%
\mode<presentation>
{
  \usetheme{default}      % or try Darmstadt, Madrid, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\title{\textbf{\huge  SENSOR NETWORKS}}
\author{B.keerthana -  21B01A6108- AIML   \\  A.pavani naga mallika - 21B01A5402-AI&DS-A \\  V.lekkhaa srii -21B01A04C0 -ECE-B\\ S.chandra kali priya - 21B01A0349- MECH \\  K.jahnavi krishna -21B01A6142 -AI&ML}

\date{4 FEBRUARY 2023}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

% Uncomment these lines for an automatically generated outline.
%\begin{frame}{Outline}
%  \tableofcontents
%\end{frame}

\section{\textit{\Huge INTRODUCTION}}

\begin{frame}{\Huge INTRODUCTION}
\begin{block}{}
\\ Samantha is working on the Amazon Carbon-dioxide Measurement (ACM) project, where a wireless sensor network in the Amazon rainforest gathers environmental information to help scientists better understand earth's complex ecosystems and the impact of human activities. To test her hypothesis, she needs to find a subset of sensors where each pair of sensors can communicate directly with each other (distance at most d). The goal is to choose as many sensors as possible, given their current locations represented as points in a two-dimensional plane, using Euclidean distance to measure the distance between two points.\\ 
\end{block}
\end{frame}



\subsection{\textbf{\Huge APPROACH}}

\begin{frame}{\Huge APPROACH}
\begin{itemize}
 \huge \item According to the problem description the sensors should be wireless and there should be communication between them. \\ \item \huge the diameter of the sensors should be less than or equal to 1.
 \end{itemize}



% Commands to include a figure:
%\begin{figure}
%\includegraphics[width=\textwidth]{your-figure's-file-name}
%\caption{\label{fig:your-figure}Caption goes here.}
%\end{figure}



\end{frame}

\subsection{\textbf{\Huge LEARNINGS}}

\begin{frame}{\Huge LEARNINGS}
\begin{block}{}
\huge 1. We were working on Gitlab.\\ \huge 2.We learnt how to work with Latex. \\ \huge 3.We learned new things by working as a team.
\end{block}


\end{frame}
\subsection{\textbf{\Huge CHALLENGES}}
\begin{frame}{\Huge CHALLENGES}
\begin{itemize} 
\huge\item We face difficulty in understanding the problem statement.\\\item \huge Difficulty in writing the code.\\ \item \huge Error correction took a lot of time. \\
\end{itemize}{}
    
\end{frame}
\subsection{\textbf{\Huge STATISTICS}}
\begin{frame}{\Huge STATISTICS}
\begin{block}{}
\huge 1.Number of lines of code = 69 \\ \huge 2.Number of Functions used =  3  
\end{block}

    
\end{frame}
\subsection{\textbf{\Huge DEMO/SCREENSHOTS}}
\begin{frame}{\Huge DEMO/SCREENSHOTS}
       \begin{figure}
           \centering
           \includegraphics[width=10cm]{output 1.jpg}
           \caption{demo screen shot}
           \label{}
       \end{figure}
     \end{frame}
     \begin{frame}{\Huge DEMO/SCREENSHOTS}
       \begin{figure}
           \centering
           \includegraphics[width=20cm]{demo1.jpg}
           \caption{demo screen shot}
           \label{}
       \end{figure}
     \end{frame}
\begin{frame}{}
\centering\Huge
\emph{THANK YOU}
\end{frame}

\end{document}