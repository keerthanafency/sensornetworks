import math
import sys  

def calculate_distance(x1,x2,y1,y2):
    return math.sqrt(((x1-x2)*(x1-x2)) + ((y1-y2)*(y1-y2)))

def is_inside_circle(c1,c2,radius,x,y):
    return ((x-c1)*(x-c1)) + ((y-c2)*(y-c2)) <= radius * radius
 
def main():

    ArgLength = len(sys.argv)
    
    n = int(sys.argv[1])
    Maximum_Distance = int(sys.argv[2])

    Points = []
    Answer = []
    X_Cord = []
    Y_Cord = []

    for i in range(3,ArgLength):
        if i % 2 == 1 :
            X_Cord.append(int(sys.argv[i]))
        else :
            Y_Cord.append(int(sys.argv[i]))
    
    # If input is entered incorrectly
    if len(X_Cord) != len(Y_Cord) :
        print("Enter Correct Input")
        sys.exit(-1)

    for i in range(len(X_Cord)):
        x = X_Cord[i]
        y = Y_Cord[i]
        Points.append((x,y))
    
    for i in range(n):
        for j in range(i+1, n):
            x1 = Points[i][0]
            y1 = Points[i][1]
            x2 = Points[j][0]
            y2 = Points[j][1]
            center_x = (x1 + x2) // 2
            center_y = (y1 + y2) // 2

            diameter = calculate_distance(x1,x2,y1,y2)

            Temp = []

            if diameter <= Maximum_Distance :
                Temp.append(i+1)
                Temp.append(j+1)

                for k in range(n):
                    if k == i or k == j:
                        continue
                    if is_inside_circle(center_x,center_y,diameter//2, Points[k][0], Points[k][1]) :
                        Temp.append(k+1)

            if len(Temp) > len(Answer) :
                Answer = Temp.copy()


    print(len(Answer))
    print(Answer)

if _name_ == "_main_":
    main()

